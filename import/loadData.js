const fs = require('fs')
const _ = require('lodash')
const iconv = require('iconv-lite')
const parseCsv = require('csv').parse
const phoneOutput = require('node-phonenumber')

const phoneUtil = phoneOutput.PhoneNumberUtil.getInstance()


const parseWorkUnit = require('./parseWorkUnit')
const parseHouse = require('./parseHouse')

/**
 * Parse a house string into an object
 *
 * @param {string} house The string with the info about the house/workUnit
 * @param {array} addressInfoList List of house id's and their adress info
 * @returns {object} Object describing the house
 */
function getHouse(house, addressInfoList) {
  const parsedHouse = parseHouse(house)

  const houseCheck = _.clone(parsedHouse)
  delete houseCheck.constructionHouseNumberAddition
  const addressInfo = addressInfoList.find((element) => (
    _.isEqual(element.id, houseCheck)
  ))

  if (!addressInfo) {
    throw new Error(`No address info found for ${house}`)
  }
  _.merge(parsedHouse, addressInfo.address)
  return parsedHouse
}

/**
 * Parse a workUnit string into an object
 *
 * @param {string} workUnit The string with the info about the workUnit
 * @param {array} addressInfoList List of workunit id's and their adress info
 * @returns {object} Object describing the workUnit
 */
function getWorkUnit(workUnit, addressInfoList) {
  const parsedWorkUnit = parseWorkUnit(workUnit)
  if (!parsedWorkUnit) {
    return null
  }

  const workUnitCheck = _.clone(parsedWorkUnit)
  delete workUnitCheck.constructionWorkUnitNumberAddition
  const addressInfo = addressInfoList.find((element) => (
    _.isEqual(element.id, workUnitCheck)
  ))

  if (!addressInfo) {
    throw new Error(`No address info found for ${workUnit}`)
  }
  _.merge(parsedWorkUnit, addressInfo.address)
  return parsedWorkUnit
}

/**
 * Parse a phone number from a string and return it
 *
 * @param {string} value The string containing 1 or 2 phone numbers
 * @param {number} which In case of multiple phone numbers,
 *                       if we should return the first or second
 * @returns {string} The phone number
 */
function parsePhoneNr(value, which) {
  const select = which - 1
  // the default
  let phone = value.split('/')[select] ? value.split('/')[select].trim() : null

  // check if extra phone number is denoted with ()
  if (value.endsWith(')')) {
    const regex = /\(.*?\)$/g
    const results = regex.exec(value)
    if (!results) {
      throw Error('Unrecognised phone format')
    }

    // the second phone number
    if (select === 0) {
      // first phone number
      phone = phone.replace(results[0], '')
      phone = phone.trim()
    } else {
      phone = results[0].trim().substring(1, results[0].trim().length - 1)
    }
  } else if (value.indexOf('/') === -1 && value.split(' ').length > 1) {
    // apperently split with a space
    const split = value.split(' ')
    if (split[0] === 'geheim') {
      return null
    }

    phone = split[select]
  }

  if (phone === null) {
    return null
  }
  return phone
}

/**
 * Get a phone number from a string and format it as E164
 *
 * @param {string} value The string containing 1 or 2 phone numbers
 * @param {number} which In case of multiple phone numbers,
 *                       if we should return the first or second
 * @returns {string} The phone number
 */
function getPhoneNr(value, which) {
  // first try to parse the phone directly
  let phone
  try {
    phone = phoneUtil.parse(phone, 'NL')
    phone = phoneUtil.format(phone, phoneOutput.PhoneNumberFormat.E164)
  } catch (ignored) {
    try {
      // ok , that did not work, lets try again with our own parser
      phone = parsePhoneNr(value.trim(), which)
      if (phone != null) {
        phone = phoneUtil.parse(phone, 'NL')
        phone = phoneUtil.format(phone, phoneOutput.PhoneNumberFormat.E164)
      }
    } catch (e) {
      throw Error(`${e}: ${phone}`)
    }
  }

  return phone
}

/**
 * Read a UTF16 excel file, convert it to utf8 and parse it as csv
 *
 * @param {string} filename Location of the file
 * @param {function} cb Callback function that is called when don
 * @returns {undefined}
 */
function readFile(filename, cb) {
  let data = ''
  const readableStream = fs.createReadStream(filename)
    .pipe(iconv.decodeStream('utf16'))

  readableStream.on('data', (chunk) => {
    data += chunk
  })

  readableStream.on('end', () => {
    parseCsv(data, {
      delimiter: '\t',
      rowDelimiter: '\n',
      trim: true,
    }, (err, result) => {
      if (err) {
        throw err
      }
      cb(result)
    })
  })
}


/**
 * Parse email from a string containing 1 or 2 emails
 *
 * @param {string} value The string containing the email(s)
 * @param {number} which In case of multiple emails wether we should
 *                       return the first or second
 * @returns {string} The email
 */
function getEmail(value, which) {
  let email1 = value[5] ? value[5] : null
  let email2 = value[6] ? value[6] : null

  if (email1 == null) {
    throw Error(`${value[2]} ${value[4]}: Email is required`)
  }

  if (email1.split('/').length > 1) {
    // they made a seperate column for email2 but
    // still use / sometimes in the first column
    email1 = value[5].split('/')[0].trim()
    email2 = value[5].split('/')[1].trim()
  }

  // oh, and lets fix spelling mistakes as well
  email1 = email1.replace('hotmailcom', 'hotmail.com')

  return (which === 1 ? email1 : email2)
}

/**
 * Load a file and parse its data to prepare it for database insertion
 *
 * @param {string} filename The location of the file
 * @param {array} addressInfo List of house id's and their adress info
 * @param {function} cb Function that gets called with the data when done
 * @returns {undefined}
 */
function parseFile(filename, addressInfo, cb) {
  readFile(filename, (data) => {
    // remove title column
    data = data.splice(1)

    // map to object
    data = data.map((value) => ({
      house: getHouse(value[0], addressInfo),
      workUnit: getWorkUnit(value[1], addressInfo),
      firstname: value[2] ? _.startCase(value[2]) : null,
      snPrefix: value[3] ? value[3] : null,
      surname: value[4] ? _.startCase(value[4]) : null,
      email: getEmail(value, 1),
      email2: getEmail(value, 2),
      phoneNr: getPhoneNr(value[7], 1),
      phoneNr2: getPhoneNr(value[7], 2),
    }))

    cb(data)
  })
}

module.exports = parseFile
