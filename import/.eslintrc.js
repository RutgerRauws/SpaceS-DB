module.exports = {
  "extends" : "../.eslintrc.js",
  "env"     : {
    "mocha" : true
  },
  "globals" : {
    "expect" : false,
    "should" : false,
    "sinon"  : false
  },
  rules: {
    'import/no-extraneous-dependencies': ['error', {'devDependencies': true}],
    'no-console': 0,
  }
}
