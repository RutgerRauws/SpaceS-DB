Download the [streetname and housenumber page](http://www.space-s.nl/voorzieningen-op-space-s/straatnaam-en-huisnummer/) as html
Then run `import/prepareHouseNumbers.js "location of html file"`

Save the excel with resident info as UTF-16.
Then run `import/index.js "location of UTF16 file"`
