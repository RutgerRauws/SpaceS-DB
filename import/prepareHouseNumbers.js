#!/usr/bin/env node

const fs = require('fs')
const cheerio = require('cheerio')
const _ = require('lodash')
const jsonfile = require('jsonfile')

const parseWorkUnit = require('./parseWorkUnit')
const parseHouse = require('./parseHouse')

const argv = require('optimist').argv._

if (argv.length > 1) {
  throw Error('Can only import one file at the time.')
}
const fileLocation = argv[0]
if (!fs.lstatSync(fileLocation).isFile()) {
  throw Error('Can not find file or it is a directory.')
}


/**
 * Parse the house info from the site
 *
 * @param {string} houseInfo The house info from the table
 * @returns {object} The parsed info
 */
function parseHouseInfo(houseInfo) {
  const info = houseInfo.split('\n')

  // street & housenumber
  const streetInfo = info[0].split(' ')
  const houseNumber = streetInfo[streetInfo.length - 1]
  delete streetInfo[streetInfo.length - 1]
  const street = streetInfo.join(' ').trim()

  // zip code
  const zipCodeInfo = info[1].split(' ')
  const city = zipCodeInfo[2]
  const zipCode = zipCodeInfo[0] + zipCodeInfo[1]

  return {
    street,
    houseNr: houseNumber,
    zipCode,
    city,
  }
}

/**
 * Parse the builders id info from the site
 *
 * @param {string} buildersId The house info from the table
 * @returns {array} All houses contained in this builders id
 */
function parseBuildersId(buildersId) {
  const info = buildersId.split('\n')
  if (info[0] === 'Geen nummer') {
    return []
  }
  if (info[1] === 'Werkruimte') {
    return [parseWorkUnit(info[0])]
  }
  if (info[1] === 'Woon- en werkruimte') {
    return [parseHouse(info[0]), parseWorkUnit(info[0])]
  }

  const studentenkamersRegex = /(\d{1,2}) studentenkamers/.exec(info[1])
  if (studentenkamersRegex !== null) {
    const studentenkamers = parseInt(studentenkamersRegex[1], 10)
    const house = parseHouse(info[0])
    const houses = Array(...new Array(studentenkamers)).map(
      (x, i) => _.merge({ roomNr: String(i + 1) }, house)
    )
    return houses
  }

  return [parseHouse(info[0])]
}

/**
 * Prepare house numbers by adding adresses to them
 *
 * @param {string} filename Location of the .html file containing the addresses
 * @returns {undefined}
 */
function prepareHouseNumbers(filename) {
  fs.readFile(filename, (err, data) => {
    if (err) {
      throw err
    }

    const $ = cheerio.load(data.toString())

    const houseMap = []

    const query = '.avia-data-table-wrap > table > tbody > ' +
      'tr:not(\'[class*=avia-heading-row]\')'
    $('.entry-content-wrapper').find(query).each((index, element) => {
      const buildersInfo = parseBuildersId($(element.children[0]).text())
      if (buildersInfo.length === 0) {
        return
      }

      const address = parseHouseInfo($(element.children[1]).text())

      buildersInfo.forEach((houseId) => {
        houseMap.push({ id: houseId, address })
      })
    })
    const outputFileName = `${__dirname}/preparedHouseNumbers.json`
    jsonfile.writeFile(outputFileName, houseMap, (error) => {
      if (error) { throw error }
    })
  })
}

prepareHouseNumbers(fileLocation)
