const yaml = require('yaml-js')
const SwaggerParser = require('swagger-parser')
const fs = require('fs')

const swaggerDir = './api/swagger'
const schema = './api/schema/index.yaml'

SwaggerParser.bundle(schema, {
  strictValidation: true,
  validateSchema: true,
}, (err, api, metadata) => {
  if (err) {
    throw err
  } else {
    fs.writeFile(`${swaggerDir}/swagger.yaml`, yaml.dump(api),
      (error) => { if (error) { throw error } }
    )
  }
})
