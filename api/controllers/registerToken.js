const models = require('../../models')
const stdGetAll = require('../helpers/std/stdGetAll')
const stdGetSpecific = require('../helpers/std/stdGetSpecific')

/**
 * Get all residents
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getRegisterTokenList(req, res) {
  return stdGetAll(models.registerToken, req, res)
}

/**
 * Get a specific resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getRegisterToken(req, res) {
  const id = req.swagger.params.id.value
  return stdGetSpecific(models.registerToken, id, req, res)
}

/**
 * Get resident from register link
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getResidentFromRegisterToken(req, res) {
  const id = req.swagger.params.id.value

  return models.registerToken.findById(id, {
    include: {
      model: models.resident,
      include: models.house,
    },
  }).then((result) => {
    // check if we found the instance
    if (result != null) {
      res.status(200).send(result.get('resident'))
    } else {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    }
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = {
  getRegisterTokenList,
  getRegisterToken,
  getResidentFromRegisterToken,
}

