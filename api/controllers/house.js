

const models = require('../../models')
const stdGetAll = require('../helpers/std/stdGetAll')
const stdGetSpecific = require('../helpers/std/stdGetSpecific')

/**
 * Get all houses
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getHouseList(req, res) {
  return stdGetAll(models.house, req, res)
}

/**
 * Get a specific house
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getHouse(req, res) {
  const id = req.swagger.params.id.value
  return stdGetSpecific(models.house, id, req, res)
}


module.exports = {
  getHouseList,
  getHouse,
}

