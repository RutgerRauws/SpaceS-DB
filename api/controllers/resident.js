const path = require('path')
const settings = require('config')

// node mailer
const nodemailer = require('nodemailer')

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport(settings.smtp.url)
const mailDefaultOptions = settings.smtp.options

const EmailTemplate = require('email-templates').EmailTemplate

const getIndex = require('../helpers/util/getIndex')
const getName = require('../helpers/util/getName')

const models = require('../../models')
const stdGetAll = require('../helpers/std/stdGetAll')
const stdGetSpecific = require('../helpers/std/stdGetSpecific')
const stdPatch = require('../helpers/std/stdPatch')
const stdGetRelation = require('../helpers/std/stdGetRelation')
const stdPost = require('../helpers/std/stdPost')
const stdPostRelation = require('../helpers/std/stdPostRelation')


const root = require('app-root-path').toString()

const template = path.join(root, 'mails', 'invite-mail')
const tryOutMail = new EmailTemplate(template)


/**
 * Get all residents
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getResidentList(req, res) {
  return stdGetAll(models.resident, req, res)
}

/**
 * Get a specific resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getResident(req, res) {
  const id = req.swagger.params.id.value
  return stdGetSpecific(models.resident, id, req, res)
}

/**
 * Create a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function addResident(req, res) {
  const body = req.swagger.params.body.value
  return stdPost(models.resident, body, req, res)
}

/**
 * Patch (change) a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function patchResident(req, res) {
  const id = req.swagger.params.id.value
  const body = req.swagger.params.body.value
  return stdPatch(models.resident, id, body, req, res)
}

/**
 * Send a registermail to a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function sendResidentRegisterMail(req, res) {
  const id = req.swagger.params.id.value
  const logger = req.log

  return models.resident.findById(id).then((resident) => {
    // check if we found the instance
    if (resident == null) {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    } else {
      models.registerToken.create({ residentId: id }).then((created) => {
        const url = `/${getName(created)}/`
        res.setHeader('Location', url + getIndex(created))

        const email = resident.get('email')

        const clientUrls = settings.client
        const baseRegisterUrl = clientUrls.baseUrl + clientUrls.registerUrl
        const registerUrl = `${baseRegisterUrl}/${getIndex(created)}`

        const replace = {
          registerLink: registerUrl,
          baseUrl: settings.server.host,
        }

        return tryOutMail.render(replace, (err, result) => {
          const mailOptions = Object.assign(mailDefaultOptions, {
            to: email,
            subject: result.subject,
            text: result.text,
            html: result.html,
          })

          // send mail with defined transport object
          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              logger.info('Error while sending mail to', resident.fullName)
              logger.error(error)
              res.status(500).send()
            } else {
              logger.info('Message sent:', info.response)
              res.status(201).send(created.toJSON())
            }
          })
        })
      })
    }
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}


/**
 * Get a the registerlink belonging to a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getRegisterToken(req, res) {
  const id = req.swagger.params.id.value
  const relation = { name: 'registerToken', model: models.registerToken }
  return stdGetRelation(models.resident, id, relation, req, res)
}

/**
 * Add a the registerlink to a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function addRegisterToken(req, res) {
  const id = req.swagger.params.id.value
  const body = { residentId: id }
  return stdPostRelation(
    models.resident, id,
    models.registerToken, body,
    req, res)
}

/**
 * Get the forumaccount belonging to a resident's account
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getForumAccount(req, res) {
  const relation = { name: 'account', model: models.account }
  const id = req.swagger.params.id.value

  const opts = {
    include: {
      model: models.account,
      include: {
        model: models.forumAccount,
      },
    },
  }
  models.resident.findById(id, opts).then((result) => {
    // check if we found the instance
    if (result != null) {
      const relationResult = result.get(relation.name)
      if (relationResult) {
        res.status(200).send(relationResult.get('forumAccount'))
      } else {
        res.status(404).send(`Item does not have a ${relation.name} attached.`)
      }
    } else {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    }
  }).catch(
      /* istanbul ignore next */
      (err) => {
        throw err
      }
    )
}

/**
 * Get the house belonging to a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getHouse(req, res) {
  const id = req.swagger.params.id.value
  const relation = { name: 'house', model: models.house }
  return stdGetRelation(models.resident, id, relation, req, res)
}

/**
 * Get a the registerlink belonging to a resident
 * Get the account belonging to a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getAccount(req, res) {
  const id = req.swagger.params.id.value
  const relation = { name: 'account', model: models.account }
  return stdGetRelation(models.resident, id, relation, req, res)
}

/**
 * Get the workunit belonging to a resident
 *
 * @param {object} req A handle to the request object
 * @param {object} res A handle to the response object
 * @return {undefined}
 */
function getWorkUnit(req, res) {
  const id = req.swagger.params.id.value
  const relation = { name: 'workUnit', model: models.workUnit }
  return stdGetRelation(models.resident, id, relation, req, res)
}

module.exports = {
  getResidentList,
  getResident,
  addResident,
  patchResident,
  sendResidentRegisterMail,
  getRegisterToken,
  addRegisterToken,
  getAccount,
  getForumAccount,
  getHouse,
  getWorkUnit,
}

