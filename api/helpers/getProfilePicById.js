const models = require('../../models')

/**
 * Get a profile picture from the database by its id
 *
 * @param {number} id The id of the profile picture
 * @returns {Promise} Promise that resolves into the id
 */
function getProfilePicById(id) {
  return models.account.findOne({
    attributes: ['id'],
    where: {
      profilePictureId: id,
    },
    include: [
      { model: models.file, as: 'profilePicture', required: true },
    ],
  }).then((result) => {
    // check if we found the instance
    if (result != null) {
      return result.profilePicture.toJSON()
    }
    // nothing found
    return null
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = getProfilePicById
