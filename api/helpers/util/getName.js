/**
 * Get the name of a created instance
 *
 * @param {object} instance The created instance
 * @returns {string} The index
 */
function getName(instance) {
  return instance.$modelOptions.name.singular
}

module.exports = getName
