const validatorFormat = require('./validatorFormatter')

/**
 * Handles the formatting of validation errors
 *
 * @param {array} errors The errors, from sequelizejs
 * @param {object} res the response, from restify
 * @param {object} logger Logger object, from restify
 * @returns {undefined}
 */
function handleValidationErrors(errors, res, logger) {
  logger.info('Validation error: ', errors)

  const errorlist = {}
  // format the errors
  errors.errors.forEach((error) => {
    errorlist[error.path] = validatorFormat(error.message)
  })

  res.status(400).send(errorlist)
  return errorlist
}

module.exports = handleValidationErrors
