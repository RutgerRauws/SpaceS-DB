const models = require('../../../models')
const q = require('q')

/**
 * Load a account from a resetPassword token
 *
 * @param {string} token The token that will be used to find the account
 * @param {logger} logger Logger used by restify
 * @param {object} opts Additional options, optional
 *
 * @return {Promise} Promise that resolves into the account
 */
function getAccountFromResetPasswordToken(token, logger, opts) {
  return new q.Promise((resolve, reject) => {
    if (opts === undefined) {
      opts = {}
      opts.include = []
    }
    if (!Array.isArray(opts.include)) {
      logger.error('Opts.include must be an array!')
    }
    opts.include.push({
      model: models.resetPasswordToken,
      where: {
        token,
      },
    })
    models.account.findOne(opts).then((account) => {
      if (account) {
        resolve(account)
      } else {
        reject()
      }
    })
  })
}

module.exports = getAccountFromResetPasswordToken
