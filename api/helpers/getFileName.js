const fs = require('fs')
const path = require('path')
const shortid = require('shortid')

/**
 * Generate a filename, makes sure we don't overwrite a file
 *
 * @param {string} location The directory with all the files
 * @param {string} extention Extention of the file
 * @returns {string} The location
 */
function getFileName(location, extention) {
  let newlocation = path.join(
    location,
    shortid.generate()
  )
  newlocation += `.${extention}`
  try {
    fs.accessSync(newlocation)
  } catch (e) {
    return newlocation
  }
  return getFileName(location, extention)
}

module.exports = getFileName
