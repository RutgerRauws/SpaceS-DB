/**
 * Get all instances of a model
 *
 * @param {object} model The model belonging to this route
 * @param {object} req Request object, from restify
 * @param {object} res Response object, from restify
 * @returns {undefined}
 */
function stdGet(model, req, res) {
  return model.findAll().then((result) => (
    res.status(200).send(result)
  )).catch(
      /* istanbul ignore next */
      (err) => {
        throw err
      }
    )
}

module.exports = stdGet
