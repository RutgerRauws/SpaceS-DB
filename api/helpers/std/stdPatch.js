/**
 * Patch a instance
 *
 * @param {object} model The model belonging to this route
 * @param {int|string} id The identifier of the instance
 * @param {object} body The new body of the object
 * @param {object} req Request object, from restify
 * @param {object} res Response object, from restify
 * @param {function} next Function to call when we're done, from restify
 * @returns {undefined}
 */
function stdPatch(model, id, body, req, res, next) {
  // eslint-disable-next-line consistent-return
  return model.findById(id).then((result) => {
    // check if we found the instance
    if (result != null) {
      return result.update(body).then(() => {
        res.status(200).send(result.toJSON())
      })
    }
    // send the error
    res.status(404).send(`Item with id (${id}) not found.`)
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = stdPatch
