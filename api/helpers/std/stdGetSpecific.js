/**
 * Get one specific instance
 *
 * @param {object} model The model belonging to this route
 * @param {int|string} id The identifier of the instance
 * @param {object} req Request object, from restify
 * @param {object} res Response object, from restify
 * @param {function} next Function to call when we're done, from restify
 * @returns {undefined}
 */
function stdGetSpecific(model, id, req, res, next) {
  return model.findById(id).then((result) => {
    // check if we found the instance
    if (result != null) {
      res.status(200).send(result.toJSON())
    } else {
      // send the error
      res.status(404).send(`Item with id (${id}) not found.`)
    }
  }).catch(
    /* istanbul ignore next */
    (err) => {
      throw err
    }
  )
}

module.exports = stdGetSpecific
