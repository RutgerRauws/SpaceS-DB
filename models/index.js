// load required libraries
const fs = require('fs')
const Sequelize = require('sequelize')
const appRoot = require('app-root-path').toString()

const db = {}
let sequelize

const settings = require('../util/loadDbConfig.js')

if (!settings.define) {
  settings.define = {}
}

// prevent sequelize from pluralizing table names
settings.define.freezeTableName = true

// update the dbModelLocation with the root pah
const dbModelLocationRel = `${appRoot}/${settings.modelLocation}`

/**
 * Import a model
 *
 * @param {string} modelName name of the model
 * @returns {object} The loaded model
 */
function importModel(modelName) {
  const model = sequelize.import(`${dbModelLocationRel}/${modelName}`)
  db[model.name] = model
  return model
}

/**
 * Initialize the sequelize models
 *
 * @param {string} logger Optional, the logger
 * @returns {Promise} Promise that resolves when done
 */
function init(logger) {
  if (logger) {
    // add logger to the options
    const sqLogger = logger.trace.bind(logger)
    settings.logging = sqLogger
  } else {
    settings.logging = false
  }

  // create the sequelize objec
  sequelize = new Sequelize(
    settings.database,
    settings.username,
    settings.password,
    settings
  )

  // load all models
  fs.readdirSync(dbModelLocationRel).filter((file) => (file.indexOf('.') !== 0) &&
      (file !== 'index.js') &&
      (file !== 'util')).forEach((file) => {
        importModel(file, db)
      })

  Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
      db[modelName].associate(db)
    }
  })

  // sync the models and return that promise
  return sequelize.sync()
}

db.Sequelize = Sequelize
db.init = init

module.exports = db
