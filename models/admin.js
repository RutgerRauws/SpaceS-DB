const _ = require('lodash')
const argon2 = require('argon2')

/**
 * Hook that hashes the password
 *
 * @param {object} instance The instance that the hook runs on
 * @returns {Promise} Promise that resolves when finished
 */
function hashPasswordHook(instance) {
  if (!instance.changed('password')) {
    return
  }
  // eslint-disable-next-line consistent-return
  return argon2.generateSalt().then((salt) => argon2.hash(instance.password, salt)
    .then((hash) => {
      instance.setDataValue('passwordHash', hash)
    })
  )
}

module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('admin', {
    userName: {
      type: DataTypes.STRING,
      comment: 'Username of the admin',
      allowNull: false,
      unique: true,
      validate: {
        // below if from http://stackoverflow.com/a/12019115
        // slightly altered (as js does not support lookbehind)
        // but should be the same functionality
        // it validates a valid username
        is: ['^(?=.+$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+[^._]$', 'i'],
        len: {
          args: [3, 30],
          msg: 'Should be between 3 and 30 characters.',
        },
      },
    },
    password: {
      type: DataTypes.VIRTUAL,
      comment: 'password of the admin',
      allowNull: false,
      validate: {
        len: {
          args: [5, 100],
          msg: 'Should be between 5 and 100 characters.',
        },
      },
    },
    passwordHash: {
      type: DataTypes.STRING,
      comment: 'password of the admin',
    },
  }, {
    comment: 'The admins, these are used for authentication.',
    hooks: {
      beforeCreate: hashPasswordHook,
      beforeUpdate: hashPasswordHook,
    },
    instanceMethods: {
      checkPassword(password) {
        return argon2.verify(this.passwordHash, password)
      },
      toJSON() {
        const excludedAttributes = [
          'password',
          'passwordHash',
        ]
        return _.omit(this.get(), excludedAttributes)
      },
    },
    classMethods: {
    },
  })

  return Admin
}
