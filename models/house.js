const defaultToJSON = require('./util/defaultToJSON')

module.exports = (sequelize, DataTypes) => {
  const House = sequelize.define('house', {
    blockNumber: {
      type: DataTypes.INTEGER,
      comment: 'The block the house resides in',
      allowNull: false,
      validate: {
        min: 0,
        max: {
          args: 7,
          msg: 'Should be 7 or lower.',
        },
      },
    },
    floorNr: {
      type: DataTypes.INTEGER,
      comment: 'The number of the floor where the house is located',
      allowNull: false,
      validate: {
        min: 0,
        max: {
          args: 15,
          msg: 'Should be 15 or lower.', // TODO: echte waarde
        },
      },
    },
    constructionHouseNumber: {
      type: DataTypes.INTEGER,
      comment: 'The house number that is used during construction',
      allowNull: false,
      validate: {
        min: 0,
      },
    },
    constructionHouseNumberAddition: {
      type: DataTypes.INTEGER,
      comment: 'Some house\'s with workunits have a addition, for example 5.0.1-2, this notes the -x, 2 in this case',
      allowNull: true,
      validate: {
        min: 0,
      },
    },
    roomNr: {
      type: DataTypes.INTEGER,
      comment: 'Number of the room of the resident, only in case of shared student housing',
      allowNull: true,
      validate: {
        min: 0,
        max: {
          args: 20,
          msg: 'Should be 20 or lower.', // TODO: echte waarde
        },
      },
    },
    houseNr: {
      type: DataTypes.INTEGER,
      comment: 'The house number that is used on the adress',
      allowNull: false,
      validate: {
        min: 0,
      },
    },
    street: {
      type: DataTypes.STRING,
      comment: 'The street that the house is on',
    },
    zipCode: {
      type: DataTypes.STRING,
      comment: 'The zipcode of the house',
      allowNull: false,
      validate: {
        is: {
          args: /\d{4}[A-Z]{2}/,
          msg: 'Is not a valid zipcode.',
        },
      },
    },
    city: {
      type: DataTypes.STRING,
      comment: 'The city of the house',
      allowNull: false,
    },
  }, {
    comment: 'The houses that are owned by a resident',
    getterMethods: {
      constructionId() {
        let constructionNumber = `${this.blockNumber}.${
          this.floorNr}.${this.constructionHouseNumber}`
        if (this.roomNr) {
          constructionNumber += `.${this.roomNr}`
        }
        if (this.constructionHouseNumberAddition) {
          constructionNumber += `-${this.constructionHouseNumberAddition}`
        }
        return constructionNumber
      },
    },
    instanceMethods: {
      toJSON: defaultToJSON,
    },
    classMethods: {
      associate(models) {
      },
    },
  })

  return House
}

