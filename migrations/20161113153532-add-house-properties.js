module.exports = {
  up(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      queryInterface.addColumn(
        'house',
        'street',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'house',
        'zipCode',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'house',
        'city',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      )])
  },

  down(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      queryInterface.removeColumn('house', 'street'),
      queryInterface.removeColumn('house', 'zipCode'),
      queryInterface.removeColumn('house', 'city'),
    ])
  },
}
