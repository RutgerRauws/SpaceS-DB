module.exports = {
  up(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      queryInterface.addColumn(
        'workUnit',
        'street',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'workUnit',
        'zipCode',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'workUnit',
        'city',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      )])
  },

  down(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      queryInterface.removeColumn('workUnit', 'street'),
      queryInterface.removeColumn('workUnit', 'zipCode'),
      queryInterface.removeColumn('workUnit', 'city'),
    ])
  },
}
