module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('houses',
      {
        id: {
          type: 'INTEGER',
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        blockNumber: {
          type: 'INTEGER',
          comment: 'The block the house resides in',
          allowNull: false,
        },
        floorNr: {
          type: 'INTEGER',
          comment: 'The number of the floor where the house is located',
          allowNull: false,
        },
        constructionHouseNumber: {
          type: 'INTEGER',
          comment: 'The house number that is used during construction',
          allowNull: false,
        },
        constructionHouseNumberAddition: {
          type: 'INTEGER',
          comment: 'Some house\'s with workunits have a addition,' +
          ' for example 5.0.1-2, this notes the -x, 2 in this case',
        },
        roomNr: {
          type: 'INTEGER',
          comment: 'Number of the room of the resident,' +
          ' only in case of shared student housing',
        },
        houseNr: {
          type: 'INTEGER',
          comment: 'The house number that is used on the adress',
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        residentId: {
          type: 'INTEGER',
          allowNull: true,
          references: {
            model: 'residents',
            key: 'id',
          },
          onDelete: 'SET NULL',
          onUpdate: 'CASCADE',
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('houses')
  },
}
