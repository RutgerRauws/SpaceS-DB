module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('registerLinks',
      {
        link: {
          type: 'VARCHAR(255)',
          comment: 'Link that can be used to register at',
          primaryKey: true,
          allowNull: false,
          unique: true,
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        residentId: {
          type: 'INTEGER',
          allowNull: true,
          references: {
            model: 'residents',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('registerLinks')
  },
}
