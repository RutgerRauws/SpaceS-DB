const models = require('../models')
const q = require('q')

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.removeColumn('account', 'usernameCaseInsensitive')
  },

  down(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'account',
      'usernameCaseInsensitive',
      {
        type: Sequelize.STRING,
        unique: true,
      }
    ).then(() => (
      models.init().then(() => (
        models.account.findAll().then((accounts) => (
          q.all(accounts.map((account) => account.update({
            username: account.get('username'),
          })))
        ))
      ))
    ))
  },
}
