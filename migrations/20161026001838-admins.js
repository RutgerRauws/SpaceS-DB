module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('admins',
      {
        id: {
          type: 'INTEGER',
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        userName: {
          type: 'VARCHAR(255)',
          comment: 'Username of the admin',
          allowNull: false,
          unique: true,
        },
        passwordHash: {
          type: 'VARCHAR(255)',
          comment: 'password of the admin',
        },
        createdAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
        updatedAt: {
          type: 'TIMESTAMP WITH TIME ZONE',
          allowNull: false,
        },
      })
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('admins')
  },
}
