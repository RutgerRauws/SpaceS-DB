module.exports = {
  up(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      // drop all old references
      queryInterface.removeColumn('forumAccounts', 'residentId'),
      queryInterface.removeColumn('tokens', 'residentId'),
      queryInterface.removeColumn('residents', 'profilePictureId'),

      // create account table
      queryInterface.createTable('accounts',
        {
          id: {
            type: 'INTEGER',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
          },
          userName: {
            type: 'VARCHAR(255)',
            comment: 'Username of the admin',
            allowNull: false,
            unique: true,
          },
          passwordHash: {
            type: 'VARCHAR(255)',
            comment: 'password of the admin',
          },
          createdAt: {
            type: 'TIMESTAMP WITH TIME ZONE',
            allowNull: false,
          },
          updatedAt: {
            type: 'TIMESTAMP WITH TIME ZONE',
            allowNull: false,
          },
          residentId: {
            type: 'INTEGER',
            allowNull: true,
            references: {
              model: 'residents',
              key: 'id',
            },
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
          profilePictureId: {
            type: 'INTEGER',
            allowNull: true,
            references: {
              model: 'files',
              key: 'id',
            },
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
        }).then(() =>
          // add the new columns
           Sequelize.Promise.all([
             queryInterface.addColumn(
              'forumAccounts',
              'accountId', {
                type: 'INTEGER',
                allowNull: true,
                references: {
                  model: 'accounts',
                  key: 'id',
                },
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
              }
            ),

             queryInterface.addColumn(
              'tokens',
              'accountId', {
                type: 'INTEGER',
                allowNull: true,
                references: {
                  model: 'accounts',
                  key: 'id',
                },
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
              }
            ),
           ])),
    ])
  },

  down(queryInterface, Sequelize) {
    return Sequelize.Promise.all([
      Sequelize.Promise.all([
        // remove the added columns
        queryInterface.removeColumn('forumAccounts', 'accountId'),
        queryInterface.removeColumn('tokens', 'accountId'),
      ]).then(() =>
        // drop the account table
         queryInterface.dropTable('accounts')),

      // add the removed columns
      queryInterface.addColumn(
        'forumAccounts',
        'residentId', {
          type: 'INTEGER',
          allowNull: true,
          references: {
            model: 'residents',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        }
      ),
      queryInterface.addColumn(
        'tokens',
        'residentId', {
          type: 'INTEGER',
          allowNull: true,
          references: {
            model: 'residents',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        }
      ),
      queryInterface.addColumn(
        'residents',
        'profilePictureId', {
          type: 'INTEGER',
          allowNull: true,
          references: {
            model: 'files',
            key: 'id',
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        }
      ),
    ])
  },
}
