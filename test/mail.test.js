const path = require('path')

// comparison lib
const chai = require('chai')
chai.use(require('chai-datetime'))
chai.use(require('chai-things'))

chai.config.includeStack = true
const expect = chai.expect

const root = require('app-root-path').toString()

describe('Test rendering of mails', () => {
  it('Should render the invite mail', () => {
    const EmailTemplate = require('email-templates').EmailTemplate
    const template = path.join(root, 'mails', 'invite-mail')
    const tryOutMail = new EmailTemplate(template)
    expect(tryOutMail).to.be.an('object')

    const replace = { registerLink: 'test' }
    return tryOutMail.render(replace, (err, result) => {
      if (err) {
        throw err
      }
      expect(result.text).to.be.a('string')
      expect(result.html).to.be.a('string')
      expect(result.subject).to.be.a('string')
    })
  })
})
