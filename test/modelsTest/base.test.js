const chai = require('chai')

const expect = chai.expect
chai.config.includeStack = true

describe('Test loading of models', () => {
  it('Models should return an object and a function', (done) => {
    const models = require('../../models')
    expect(models.init).to.be.an('function')
    expect(models.Sequelize).to.be.a('function')

    // try and sync
    models.init().then(() => done()).catch((err) => done(err))
  })
})
