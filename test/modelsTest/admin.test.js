const chai = require('chai')
chai.use(require('chai-string'))
chai.use(require('chai-as-promised'))

const expect = chai.expect
chai.config.includeStack = true

const models = require('../../models')

describe('Test the admin model', () => {
  it('Should hash the password and not put in the db', () =>
    models.init().then(() => models.admin.build({
      userName: 'Username',
      password: 'PasswordTest',
    }).save().then((admin) => {
      expect(admin.passwordHash).to.startWith('$argon2i$v=')

      return models.admin.findById(admin.id).then((admin2) => {
        expect(admin2.toJSON()).to.not.have.property('password')
        expect(admin2.passwordHash).to.startWith('$argon2i$v=')
      })
    }))
  )

  it('Should update the hash only when the password is updated', () =>
    models.init().then(() => models.admin.create({
      userName: 'Username2',
      password: 'PasswordTest',
    }).then((admin) => {
      const oldPassword = admin.passwordHash
      expect(oldPassword).to.startWith('$argon2i$v=')
      return admin.update({
        userName: 'Username3',
        password: 'PasswordTest',
      }).then(() => {
        // check only username changed
        expect(admin.userName).to.equal('Username3')
        expect(admin.passwordHash).to.equal(oldPassword)
        return admin.update({
          password: 'PasswordTest2',
        }).then(() => {
          // check the password changed
          expect(admin.passwordHash).to.not.equal(oldPassword)
        })
      })
    }))
  )

  it('Two instances with the same password should have unique hashes', () => {
    const data = {
      userName: 'Username4',
      password: 'PasswordTest',
    }
    return models.init().then(() => models.admin.create(data)
      .then((admin) => {
        data.userName += '_2'
        return models.admin.create(data)
          .then((admin2) => {
            expect(admin.password).to.equal(admin2.password)
            expect(admin.passwordHash).to.not.equal(admin2.passwordHash)
          })
      })
    )
  })

  it('Should validate a correct passwod', () => {
    const data = {
      userName: 'Username5',
      password: 'PasswordTest',
    }
    return models.init().then(() => models.admin.create(data)
      .then((admin) => {
        const correctPassword = admin.checkPassword(data.password)
        const incorrectPassword = admin.checkPassword(`${data.password}1`)
        return Promise.all([
          expect(correctPassword).to.eventually.be.true,
          expect(incorrectPassword).to.eventually.be.false,
        ])
      })
    )
  })
})
