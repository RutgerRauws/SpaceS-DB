module.exports = {
  dbSpecificProperties: [
    'id',
    'createdAt',
    'updatedAt',
    'accountId',
  ],
  dateProperties: [],
  examples: [
    {
      userId: 2,
    },
    {
      userId: 1,
    },
    {
      userId: 4,
    },
  ],
}
