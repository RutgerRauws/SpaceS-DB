module.exports = {
  dbSpecificProperties: [
    'createdAt',
    'updatedAt',
    'token',
    'residentId',
  ],
  dateProperties: [],
  examples: [{}, {}, {}],
}
