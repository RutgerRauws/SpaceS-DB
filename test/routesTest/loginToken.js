const insertExample = require('../util/insertExample')
const checkExample = require('../util/checkExample')
const createAccountAndLoginToken = require('../util/createAccountAndLoginToken')
const prepareTestData = require('../util/prepareTestData')
const flarumClient = require('flarum-client')

const logging = require('../../util/logging')
const settings = require('config')

const logger = logging.createLogger(settings.logs)
const deleteUser = flarumClient.deleteUser
const getUser = flarumClient.getUser
const createUser = flarumClient.createUser


const nameOfRoute = 'loginToken'

const testUser = {
  username: 'TestLoginTokenUser',
  password: 'abcDEFaa',
}

/**
 * Test inserting a object using a route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testInsert(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )

  const locationExpect = new RegExp(`/${nameOfRoute}/(\\d)*`)

  return createAccountAndLoginToken(models).then((acctkn) => {
    const account = acctkn.account
    const resident = acctkn.resident
    // create a flarum user
    return createUser(
      testUser.username,
      testUser.password,
      resident.email,
      resident,
      logger).then((result) => models.forumAccount.create({
        userId: result.id,
      })
      .then((forumAccount) => account.setForumAccount(forumAccount).then(() => {
        testData.username = testUser.username
        testData.password = testUser.password
        return insertExample(
              testData,
              url,
              `/${nameOfRoute}`,
              locationExpect,
              expect
            ).then((id) => {
              // remove added testdata
              delete testData.username
              delete testData.password

              return checkExample(
                id,
                models[nameOfRoute],
                url,
                `/${nameOfRoute}`,
                data[nameOfRoute],
                testData,
                expect,
                false
              )
            })
      })))
  })
}

/**
 * Test inserting a object using a route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testLoginDifferentCase(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )

  const locationExpect = new RegExp(`/${nameOfRoute}/(\\d)*`)

  return createAccountAndLoginToken(models).then((acctkn) => {
    const account = acctkn.account
    const resident = acctkn.resident
    // create a flarum user
    return createUser(
      testUser.username.toLowerCase(),
      testUser.password,
      resident.email,
      resident,
      logger).then((result) => models.forumAccount.create({
        userId: result.id,
      })
      .then((forumAccount) => account.setForumAccount(forumAccount).then(() => {
        testData.username = testUser.username.toUpperCase()
        testData.password = testUser.password
        return insertExample(
              testData,
              url,
              `/${nameOfRoute}`,
              locationExpect,
              expect
            ).then((id) => {
              // remove added testdata
              delete testData.username
              delete testData.password

              return checkExample(
                id,
                models[nameOfRoute],
                url,
                `/${nameOfRoute}`,
                data[nameOfRoute],
                testData,
                expect,
                false
              )
            })
      })))
  })
}

module.exports = function testLoginToken(url, data, models, expect) {
  describe(nameOfRoute, () => {
    describe('With real forum user', function withRealForumUser() {
      before(function checkForCI() {
        // these tests will fail in CI as it needs flarum credentials
        if (process.env.CI) {
          this.skip()
        }
      })

      // these tests also need a bit more time
      this.timeout(20000)

      it('should insert', () => (
        testInsert(url, data, models, expect)
      ))

      it('should login with different case', () => (
        testLoginDifferentCase(url, data, models, expect)
      ))

      afterEach(() => (
        getUser(testUser.username, logger).then((user) => (
          deleteUser(user.id, logger)
        ))
      ))
    })
  })
}
