const fs = require('fs')
const q = require('q')
const request = require('supertest')

const checkExample = require('../util/checkExample')
const checkMultiple = require('../util/checkMultiple')
const createAccountAndLoginToken =
  require('../util/createAccountAndLoginToken')
const prepareTestData = require('../util/prepareTestData')

const settings = require('config')

const nameOfRoute = 'profilePicture'

/**
 * Empty a directory
 * from https://gist.github.com/liangzan/807712#gistcomment-337828
 * edited the style
 * also removed the last line so it does not remove the dir itself
 *
 * @param {string} dirPath path to the directory
 * @returns {undefined}
 */
function emptyDir(dirPath) {
  let files = []
  try {
    files = fs.readdirSync(dirPath)
  } catch (e) {
    return
  }
  if (files.length > 0) {
    for (let i = 0; i < files.length; i += 1) {
      // do not delete hidden files
      if (!/^\..*/.test(files[i])) {
        const filePath = `${dirPath}/${files[i]}`
        if (fs.statSync(filePath).isFile()) {
          fs.unlinkSync(filePath)
        }
      }
    }
  }
}


/**
 * Test inserting a object using a route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testInsert(url, data, models, expect) {
  return new q.Promise((resolve, reject, notify) => {
    const testData = prepareTestData(
      data[nameOfRoute].examples[0],
      data[nameOfRoute]
    )

    const locationExpect = new RegExp(`/${nameOfRoute}/(\\d)*`)
    const file = `./test/upload/filesToUpload/${testData.uploadFrom}`
    // remove the uploadFrom location parameter
    delete testData.uploadFrom

    return createAccountAndLoginToken(models).then((acctkn) => {
      const account = acctkn.account
      const token = acctkn.loginToken
      request(url)
        .post(`/account/${account.get('id')}/${nameOfRoute}`)
        .attach('file', file)
        .send()
        .set('Accept', 'application/json')
        .set('loginToken', token.get('token'))
        .expect(201)
        .expect('Location', locationExpect)
        .end((err, res) => {
          if (err) {
            reject(err)
            return
          }

          testData.name = testData.name.replace('{id}', acctkn.id)

          const resp = res.body
          expect(resp).to.be.empty
          const location = res.headers.location

          const re = /\/(\d*?)$/
          const id = re.exec(location)[1]

          checkExample(
            id,
            models.file,
            url,
            `/${nameOfRoute}`,
            data[nameOfRoute],
            testData,
            expect
          ).then(() => (resolve(testData.name))).catch(reject)
        })
    }, reject)
  })
}


/**
 * Test what happens when we call the file route without attaching files
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the file route
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testNoFiles(url, data, models, expect) {
  return createAccountAndLoginToken(models).then((acctkn) => {
    const account = acctkn.account
    const token = acctkn.loginToken

    request(url)
      .post(`/account/${account.get('id')}/${nameOfRoute}`)
      .field('test', 'no file')
      .send()
      .set('Accept', 'application/json')
      .set('loginToken', token.get('token'))
      .expect(400)
      .end((err, res) => (
        new q.Promise((resolve, reject, notify) => {
          if (err) {
            reject(err)
          } else {
            const resp = res.body
            expect(resp.errors[0].code).to.eql('INVALID_REQUEST_PARAMETER')
            resolve()
          }
        })
      ))
  })
}


/**
 * Test what happens when we try and insert multiple objects using the route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the file route
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testBulkOutput(url, data, models, expect) {
  // prepare thet data for testInsert
  // (that always takes the first element in the examples array)
  const data1 = JSON.parse(JSON.stringify(data))
  const data2 = JSON.parse(JSON.stringify(data))
  // data1 needs to contain both elements because we use it to check
  data1[nameOfRoute].examples = data[nameOfRoute].examples.slice(1, 3)
  data2[nameOfRoute].examples = data[nameOfRoute].examples.slice(2, 3)

  const dataCheck = JSON.parse(JSON.stringify(data1))
  dataCheck[nameOfRoute].examples.forEach((testData) => {
    delete (testData.uploadFrom)
  })

  // insert a few works
  return new q.Promise((resolve, reject, notify) => {
    testInsert(url, data1, models, expect).then((name1) => {
      testInsert(url, data2, models, expect).then((name2) => {
        dataCheck[nameOfRoute].examples[0].name = name1
        dataCheck[nameOfRoute].examples[1].name = name2

        // check if both thing where inserted correctly
        q.all([
          models.file.findOne({ where: { name: name1 } }),
          models.file.findOne({ where: { name: name2 } }),
        ]).then((insertedObjects) => checkMultiple(
            insertedObjects,
            url,
            `/${nameOfRoute}/`,
            dataCheck[nameOfRoute],
            expect
          ).then(resolve).catch(reject)).catch(reject)
      }).catch(reject)
    }).catch(reject)
  })
}


module.exports = function testProfilePicture(url, data, models, expect) {
  describe(nameOfRoute, () => {
    it('should insert & return the newly created URI', () => (
      testInsert(url, data, models, expect)
    ))

    it('should test correct behaviour when no files attached', () => (
      testNoFiles(url, data, models, expect)
    ))

    it('should output all info', () => (
      testBulkOutput(url, data, models, expect, nameOfRoute)
    ))

    afterEach(() => (
      // delete leftover files that where uploaded
      emptyDir(settings.profileIconLocation)
    ))
  })
}
