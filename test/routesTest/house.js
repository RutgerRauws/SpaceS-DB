const insertExampleNoPost = require('../util/insertExampleNoPost')
const insertMultiple = require('../util/insertMultiple')
const checkExample = require('../util/checkExample')
const checkMultiple = require('../util/checkMultiple')
const checkBadExampleNoPost = require('../util/checkBadExampleNoPost')
const prepareTestData = require('../util/prepareTestData')

const nameOfRoute = 'house'

/**
 * Test getting a single object
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testGetOne(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )

  return insertExampleNoPost(
    models[nameOfRoute],
    testData
  ).then((id) => (
    checkExample(
      id,
      models[nameOfRoute],
      url,
      `/${nameOfRoute}`,
      data[nameOfRoute],
      testData,
      expect
    )
  ))
}

/**
 * Test what happens when we try and insert multiple objects using the route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the file route
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testBulkOutput(url, data, models, expect) {
  // slice to start from the 2nd element
  const dataList = prepareTestData(
    data[nameOfRoute].examples,
    data[nameOfRoute]
  ).slice(1)

  // insert a few works
  return insertMultiple(models[nameOfRoute], dataList)
  .then((result) => checkMultiple(
      result,
      url,
      `/${nameOfRoute}/`,
      data[nameOfRoute],
      expect
    ))
}

/**
 * Test what happens when we try and insert objects that don't validate
 *
 * @param {object} badExample Part of the testdata containg the bad data
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @param {number} n The index of the bad example
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testBadExamplesNoPost(badExample, models, expect, n) {
  it(`should give descriptive errors #${n}`, () => (
    checkBadExampleNoPost(badExample, models[nameOfRoute], expect)
  ))
}

module.exports = function testHouse(url, data, models, expect) {
  describe(nameOfRoute, () => {
    it('should get one instance', () => (
      testGetOne(url, data, models, expect)
    ))

    it('should output all info', () => (
      testBulkOutput(url, data, models, expect)
    ))

    // test all bad examples
    data[nameOfRoute].badExamples.forEach((badExample, n) => {
      testBadExamplesNoPost(badExample, models, expect, n)
    })
  })
}
