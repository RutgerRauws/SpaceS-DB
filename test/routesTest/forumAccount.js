const insertMultiple = require('../util/insertMultiple')
const checkMultiple = require('../util/checkMultiple')
const prepareTestData = require('../util/prepareTestData')


const nameOfRoute = 'forumAccount'

/**
 * Test what happens when we try and insert multiple objects using the route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the file route
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testBulkOutput(url, data, models, expect) {
  // slice to start from the 2nd element
  const dataList = prepareTestData(
    data[nameOfRoute].examples,
    data[nameOfRoute]
  ).slice(1)

  // insert a few works
  return insertMultiple(models[nameOfRoute], dataList)
  .then((result) => checkMultiple(
      result,
      url,
      `/${nameOfRoute}/`,
      data[nameOfRoute],
      expect
    ))
}

/*
function testPatching() {
  // test patching
  return new q.Promise(function(resolve, reject) {
    const username = testData.username;
    const password = testData.password;
    getUserToken(username, password, logger)
      .then(function(tokenAndId) {
        const tokenString = tokenAndId.token;
        models.token.create({token: tokenString})
          .then(function(token) {
            account.setToken(token).then(function() {
              request(url)
                .patch('/forumAccount/' + forumAccount.id)
                .send({bio: 'new bio' })
                .set('loginToken', token.token)
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                  if (err) {
                    return reject(err);
                  }
                  expect(res.body).to.be.empty;
                  getUser(username, logger).then(function(user) {
                    const attrbs = user.attributes;
                    expect(attrbs.bio).to.equal('new bio');
                    resolve();
                  }).catch(reject);
                });
            }).catch(reject);
          }).catch(reject);
      }).catch(reject);
  });
}*/

module.exports = function testForumAccount(url, data, models, expect) {
  describe(nameOfRoute, () => {
    it('should patch the account', () => (
      testBulkOutput(url, data, models, expect)
    ))

    it('should output all info', () => (
      testBulkOutput(url, data, models, expect)
    ))
  })
}
