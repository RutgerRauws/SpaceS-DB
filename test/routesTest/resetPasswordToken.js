const request = require('supertest')
const q = require('q')

const insertExampleNoPost = require('../util/insertExampleNoPost')
const insertMultiple = require('../util/insertMultiple')
const checkExample = require('../util/checkExample')
const checkMultiple = require('../util/checkMultiple')
const prepareTestData = require('../util/prepareTestData')

const nameOfRoute = 'resetPasswordToken'

/**
 * Test getting a single object
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the model to which the route gives access
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testGetOne(url, data, models, expect) {
  const testData = prepareTestData(
    data[nameOfRoute].examples[0],
    data[nameOfRoute]
  )

  return insertExampleNoPost(
    models[nameOfRoute],
    testData,
    'token'
  ).then((id) => checkExample(
      id,
      models[nameOfRoute],
      url,
      `/${nameOfRoute}`,
      data[nameOfRoute],
      testData,
      expect
    ))
}

/**
 * Test what happens when we try and insert multiple objects using the route
 *
 * @param {string} url Url of the server
 * @param {object} data Testdata of the file route
 * @param {object} models The sequelize models
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testBulkOutput(url, data, models, expect) {
  // slice to start from the 2nd element
  const dataList = prepareTestData(
    data[nameOfRoute].examples,
    data[nameOfRoute]
  ).slice(1)

  // insert a few works
  return insertMultiple(models[nameOfRoute], dataList)
  .then((result) => checkMultiple(
      result,
      url,
      `/${nameOfRoute}/`,
      data[nameOfRoute],
      expect
    ))
}

/**
 * Test getting a non existing resetPasswordtoken
 *
 * @param {string} url Url of the server
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done or rejects on error
 */
function testError(url, expect) {
  return new q.Promise((resolve, reject) => {
    request(url)
      .get(`/${nameOfRoute}/nonexisting`)
      .set('Accept', 'application/json')
      .auth('testuser', 'testpassword')
      .expect(404)
      .end((err, res) => {
        if (err) {
          reject(err)
        } else {
          const errors = res.body
          expect(errors).to.be.an('object')
          resolve()
        }
      })
  })
}


module.exports = function testResetPasswordToken(url, data, models, expect) {
  describe(nameOfRoute, () => {
    it('should get one instance', () => (
      testGetOne(url, data, models, expect)
    ))

    it('should output all info', () => (
      testBulkOutput(url, data, models, expect)
    ))

    it('should error', () => (
      testError(url, expect)
    ))
  })
}
