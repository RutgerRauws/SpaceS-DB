const request = require('supertest')
const sanitize = require('../util/sanitize')
const getSanitizedExample = require('../util/getSanitizedExample')
const q = require('q')

/**
 * Check if something was inserted correctly by requesting it from the server
 *
 * @param {string} url Url of server
 * @param {string} suburl Url of the route
 * @param {object} data Testdata of the inserted model
 * @param {object} testData Data to be inserted
 * @param {function} expect Chai expect
 * @param {boolean} multiple Whether the route returns
 *                  an array of objects (it tests the 1st)
 * @returns {Promise} Promise that resolves when done and rejects on error
 */
function checkByRequesting(
  url,
  suburl,
  data,
  testData,
  expect,
  multiple
) {
  return new q.Promise((resolve, reject, notify) => {
    request(url)
    .get(suburl)
    .set('Accept', 'application/json')
    .auth('testuser', 'testpassword')
    .expect(200)
    .end((err, res) => {
      if (err) {
        reject(err)
        return
      }
      let dataTest = res.body
      if (multiple) {
        dataTest = dataTest[0]
      }
      // test for the database specific atttibutes
      const DBSpecificProps = data.DBSpecificProperties
      if (DBSpecificProps) {
        expect(dataTest).to.include.keys(DBSpecificProps)
      }

      // remove the database-specific attributes
      dataTest = sanitize(dataTest, data)

      const testDataSanitized = getSanitizedExample(testData, data)

      expect(dataTest).to.eql(testDataSanitized)
      resolve()
    })
  })
}

module.exports = checkByRequesting
