const q = require('q')
const request = require('supertest')

/**
 * Try to insert a bad example and check if the returned errors are correct
 *
 * @param {object} badExample The bad example to try and insert
 * @param {string} url Url of server
 * @param {string} suburl Url of the route
 * @param {function} expect Chai expect
 * @returns {Promise} Promise that resolves when done and rejects on error
 */
function checkBadExample(badExample, url, suburl, expect) {
  return new q.Promise((resolve, reject, notify) => {
    request(url)
    .post(suburl)
    .send(badExample.values)
    .set('Accept', 'application/json')
    .auth('testuser', 'testpassword')
    .expect(400)
    .end((err, res) => {
      if (err) {
        return reject(err)
      }

      const resp = res.body

      // check all errors
      expect(resp).to.deep.equal(badExample.problems)

      return resolve()
    })
  })
}

module.exports = checkBadExample
