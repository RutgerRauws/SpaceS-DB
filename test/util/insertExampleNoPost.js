/**
 * Insert a example object into the database using the api
 *
 * @param {object} model The model that we insert instances of
 * @param {object} data The testdata
 * @param {string} primaryKey The primary key to send back, defaults to id
 * @returns {Promise} Promise that resolves when done and rejects on error
 */
function insertExampleNoPost(model, data, primaryKey) {
  const primary = primaryKey || 'id'
  return model.create(data).then((result) => (result.get(primary)))
}

module.exports = insertExampleNoPost
