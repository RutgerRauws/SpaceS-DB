/**
 * Get and sanitize an example
 *
 * @param {object} example The example to sanitize
 * @param {object} data The testdata belonging to the model that we
 *                      want the examples from
 * @returns {object} The sanitized object
 */
function getSanitizedExample(example, data) {
  const creationProperties = data.creationProperties
  const dateProperties = data.creationProperties || []

  // remove the database-specific attributes
  if (creationProperties) {
    creationProperties.forEach((property) => {
      delete example[property]
    })
  }

  // convert the date's
  dateProperties.concat(['updatedAt', 'createdAt']).forEach((key) => {
    if ({}.hasOwnProperty.call(example, key)) {
      example[key] = new Date(example[key])
    }
  })

  return example
}

module.exports = getSanitizedExample
