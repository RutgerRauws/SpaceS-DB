const q = require('q')
const randomstring = require('randomstring')
/**
 * Create a account, resident and a login token for use in tests
 *
 * @param {object} models The sequelize models
 * @returns {Promise} Promise that resolves into a object containing
 *                    the id, resident,  account and its token in the
 *                    form of { id: string, resident: object,
 *                    account:object, loginToken: object}
 */
function createAccountAndLoginToken(models) {
  const id = randomstring.generate(7)
  const residentData = {
    firstname: `testResident-${id}`,
    surname: 'surname',
    initials: 'T.S.',
    email: 'testResident@test.com',
  }
  const accountData = {
    username: `testAccount${id}`,
    password: 'testPassword',
  }
  const tokenString = `testToken-${id}`

  const data = {}
  const createModels = [
    models.resident.create(residentData),
    models.account.create(accountData),
    models.loginToken.create({ token: tokenString }),
  ]

  return q.all(createModels).then((results) => {
    data.id = id
    data.resident = results[0]
    data.account = results[1]
    data.loginToken = results[2]

    const setRelations = [
      data.resident.setAccount(data.account),
      data.account.setLoginToken(data.loginToken),
    ]

    return q.all(setRelations).then(() => data, (err) => { throw err })
  }, (err) => { throw err })
}

module.exports = createAccountAndLoginToken
